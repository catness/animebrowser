# About the project

**AnimeBrowser** is an example inspired by [Android Kotlin Fundamentals: styles & themes, material design etc](
https://codelabs.developers.google.com/codelabs/kotlin-android-training-styles-and-themes/). It implements part of the [Jikan - an Unofficial MyAnimeList API](https://jikan.docs.apiary.io/), specifically, searching for anime and manga by keywords and/or genres, with several sorting options. 

The app features day and night mode saved in the preferences (thanks to this wonderful [Dark Mode Preferences Tutorial](https://dev.to/aurumtechie/implement-dark-night-mode-in-an-android-app-with-a-listpreference-toggle-2k5i)  ), 3 different kinds of chips (both static and dynamically generated), and a few more Material Design attributes. I admit some of my design decisions are questionable, especially the footer with the "more" button (didn't get around to the infinite scroll yet), and the placement of "ascending/descending" sort order.

Results produced by the API search are somewhat weird, and many titles seem to be missing, but from all my tests, the standalone API version returns the same results. Maybe their database is not perfect.

I decided not to implement the offline caching because it doesn't make much sense when searching for different items every time. 

The apk is here: [animebrowser.apk](apk/animebrowser.apk).

_UPDATE_: Here's a new version with paging: [AnimeBrowserPaging](https://bitbucket.org/catness/animebrowser_paging).
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)



