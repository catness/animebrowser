package org.catness.animebrowser.results

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


class ResultsViewModelFactory(
        private val searchString : String?,
        private val type : String,
        private val sortBy : String,
        private val sort : String,
        private val genre : String?
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ResultsViewModel::class.java)) {
            return ResultsViewModel(searchString, type, sortBy, sort, genre) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}