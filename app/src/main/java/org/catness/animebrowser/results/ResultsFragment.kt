package org.catness.animebrowser.results

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat.getSystemService
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import org.catness.animebrowser.R
import org.catness.animebrowser.R.layout.fragment_results
import org.catness.animebrowser.animeDebugString
import org.catness.animebrowser.databinding.FragmentResultsBinding
import org.catness.animebrowser.network.PAGESIZE
import java.util.*

class ResultsFragment : Fragment() {
    private val LOG_TAG: String = this.javaClass.simpleName

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentResultsBinding = DataBindingUtil.inflate(
            inflater, fragment_results, container, false
        )

        val application = requireNotNull(this.activity).application

        val arguments = arguments?.let { ResultsFragmentArgs.fromBundle(it) }
        var searchString = arguments?.searchString
        var type = requireArguments().getString("type")
        var sortBy = requireArguments().getString("sortBy")
        var sort = requireArguments().getString("sort")
        var genre = arguments?.genre
        Log.i(LOG_TAG, "search ${searchString} type ${type} sortBy ${sortBy} sort ${sort}")

        val viewModelFactory = ResultsViewModelFactory(searchString, type!!, sortBy!!, sort!!, genre)

        // Get a reference to the ViewModel associated with this fragment.
        val viewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(ResultsViewModel::class.java)

        // Allows Data Binding to Observe LiveData with the lifecycle of this Fragment
        binding.lifecycleOwner = this

        // Giving the binding access to the OverviewViewModel
        binding.viewModel = viewModel
        val adapter = ResultsAdapter(ResultsAdapter.OnClickListener {
            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse(it.url)
            startActivity(openURL)
        })
        binding.listItemAnime.adapter = adapter
/*
  // this doesn't work - for some reason, this observer is triggered only once

        viewModel.anime.observe(viewLifecycleOwner, Observer { it ->
            it?.let {
                adapter.submitList(it)
                adapter.notifyDataSetChanged()
                Log.i(LOG_TAG,"Data set changed")
                Log.i(LOG_TAG, animeDebugString(it))
            }
        })
*/
        viewModel.updated.observe(viewLifecycleOwner, Observer { it ->
            Log.i(LOG_TAG,"updated = $it")
            if (it) {
                Log.i(
                    LOG_TAG,
                    animeDebugString(Collections.unmodifiableList(viewModel.anime.value))
                )
                adapter.submitList(viewModel.anime.value)
                adapter.notifyDataSetChanged()
                if (viewModel.anime.value!!.size > PAGESIZE) {
                    binding.listItemAnime.scrollToPosition(viewModel.anime.value!!.size - PAGESIZE + 1)
                }

           }
        })

        return binding.root
    }


}