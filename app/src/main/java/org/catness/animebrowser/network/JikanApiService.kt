package org.catness.animebrowser.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

enum class ApiStatus { LOADING, ERROR, DONE }

private const val BASE_URL = "https://api.jikan.moe/v3/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    //   .addConverterFactory(ScalarsConverterFactory.create())
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()

// the max page size for the API is 50
val PAGESIZE: Int = 20

interface JikanApiService {
    @GET("search/{type}/")
    fun getAnime(
        @Path("type") type: String,
        @Query("q") search: String?,
        @Query("order_by") sortBy: String,
        @Query("sort") sort: String,
        @Query("genre") genre: String?,
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ):
            Deferred<AnimeFeed>

}

object JikanApi {
    val retrofitService: JikanApiService by lazy {
        retrofit.create(JikanApiService::class.java)
    }
}
