package org.catness.animebrowser.network

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize


data class AnimeNet(
    val title: String,
    val type: String,
    val url: String,
    @Json(name = "image_url") val imageUrl: String,
    val synopsis: String,
    @Json(name = "start_date") val startDateFull: String? = "",
    @Json(name = "end_date") val endDateFull: String? = "",
    val episodes: Int? = 0,
    val chapters: Int? = 0,
    val volumes: Int? = 0,
    val score: Float? = 0f,
    val mal_id: Int
) {
     val malId
        get() = mal_id.toString()  // for debugging
    val startDate
        get() = startDateFull?.substring(0,10) ?: ""
    val endDate
        get() = endDateFull?.substring(0,10) ?: ""


}

data class AnimeFeed(
    val results: List<AnimeNet>
)
