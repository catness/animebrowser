package org.catness.animebrowser

import org.catness.animebrowser.network.AnimeNet

fun animeDebugString(anime: List<AnimeNet>) : String {
    return anime.joinToString { it.malId + " " + it.title }
}